---
title: GROW Profile
author: Alan Wong
date: 6/10/2019
LinkedIn: https://www.linkedin.com/in/alan-wong-bb342569
---

# Bio

Hello, World! I am a human specializing in human-machine interactions
and the languages used to systematize those interactions. Usually,
this means building websites and writing scripts to boss around
machines. I like good documentation because it is how we pass on
know-how and because sometimes I forget how to do things. Welp!

When I'm not pressing plastic keycaps, I like to interact with fellow
humans, draw pictures, walk long distances, and play stringed
instruments.

# Skills

- Web App Development
- Open Source Software
